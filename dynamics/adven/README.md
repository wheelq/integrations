### wq-id

`eyJ1dWlkIjogInViWUoyZEMzOUthVTd1TUJBc0NoZnNaUDdZQUhXVSIsCiJ0ZW5hbnQiOiAiYWR2ZW5fYXBpIiwKInVzZXJuYW1lIjogImFwaSJ9`

### How to generate one

* **NEVER** use credentials of any "actual" user! Create a new user for this.

* create a JSON like this:

```{"uuid": "password", "tenant": "tenantname", "username": "username"}```

> `uuid` is just a fake name for a password field

* paste into a [Base64 encoder](https://www.base64encode.org/)

* copy

> You can [decode](https://www.base64encode.org/) the token to get legible login info